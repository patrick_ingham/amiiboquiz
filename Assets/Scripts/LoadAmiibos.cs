﻿using UnityEngine;
using UnityEngine.UI;
using System.Xml;
using System.IO;
using System.Collections.Generic;
using UnityEngine.SceneManagement;

public class LoadAmiibos : MonoBehaviour
{
	public List<Amiibo> amiibos = new List<Amiibo>();
	private int counter = 0;
	public Image amiiboImageHolder;
	private Amiibo currentAmiibo;
	public Text[] answers = new Text[3];
	public Text scoreBox;
	public int score = 0;

	void Start()
	{
		TextAsset textData = Resources.Load<TextAsset>("Amiibos");
		parseAmiibosXML(textData.text);

		selectNextAmiibo(0);
	}

	private void parseAmiibosXML(string xmlData)
	{
		XmlDocument xmlDoc = new XmlDocument();
		xmlDoc.Load(new StringReader(xmlData));

		string xmlPathPattern = "//amiibos/amiibo";
		XmlNodeList myNodeList = xmlDoc.SelectNodes(xmlPathPattern);
		
		foreach (XmlNode node in myNodeList)
		{
			Amiibo amiibo = new Amiibo(int.Parse(node.Attributes["identifier"].Value), node.Attributes["image"].Value, node.InnerText);
			amiibos.Add(amiibo);
		}

	}

	public void selectNextAmiibo(int selected)
	{
		if (counter > 0) checkAnswer(selected);
		currentAmiibo = amiibos[Random.Range(0, amiibos.Count)];

		setCanvasElements();

		counter++;
	}

	private void checkAnswer(int selected)
	{
		if (answers[selected].text.CompareTo(currentAmiibo.FullName) == 0)
		{
			score++;
		}

	}

	private void setCanvasElements()
	{
		addRandomNamesToAnswerButtons();
		amiiboImageHolder.sprite = currentAmiibo.AmiiboSprite;
		int correctAnswer = Random.Range(0, 2);
		answers[correctAnswer].text = currentAmiibo.FullName;
		scoreBox.text = "Score " + score + "/"+counter;

		
	}

	private void addRandomNamesToAnswerButtons()
	{
		foreach(Text answer in answers)
		{
			answer.text = amiibos[Random.Range(0, amiibos.Count)].FullName;
		}
	}

}