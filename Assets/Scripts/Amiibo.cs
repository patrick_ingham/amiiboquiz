﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Amiibo {

	public int Identifier { get; }
	public string FullName { get; }
	public Sprite AmiiboSprite { get; }

	public Amiibo(int identifier,  string spriteName, string fullName) {
		Debug.Log("created");
		this.Identifier = identifier;
		this.FullName = fullName;
		this.AmiiboSprite = Resources.Load<Sprite>("images/" + spriteName);
	}

	
}

